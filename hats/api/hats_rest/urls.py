from django.urls import path
from .views import api_list_hats, api_show_hat, api_list_locationVOs


urlpatterns = [
    path("hats/", api_list_hats, name="api_hats"),
    path("hats/<int:pk>/", api_show_hat, name="api_hat"),
    path("location_value_objects/", api_list_locationVOs, name="api_location_vos"),  # noqa
]
