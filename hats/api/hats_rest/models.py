from django.db import models
from django.urls import reverse


# Create your models here.
class LocationVO(models.Model):
    """
    The location VO is how we'll connect our hats to our wardrobe microservice.
    """
    import_id = models.IntegerField(unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)

    def get_api_url(self):
        return reverse("api_location", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.closet_name} - {self.section_number}/{self.shelf_number}"  # noqa

    class Meta:
        ordering = ("closet_name", "section_number", "shelf_number")


class Hat(models.Model):
    """
    The hat model describes a hat. Hats have a relationship with locations in a wardrobe.  # noqa
    """

    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    picture_url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="+",  # do not create a related name on State
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})

    def __str__(self):
        return self.style_name

    class Meta:
        ordering = ("style_name",)  # Default ordering for Location
