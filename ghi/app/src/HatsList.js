import React, { useEffect, useState } from 'react';

function HatsList(props) {

    const [hats, setHats] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8090/api/hats/";
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setHats(data.hats)
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = async (id) => {

        const response = await fetch('http://localhost:8090/api/hats/'+id, {method: "delete"});
        if (response.ok) {
            const result = hats.filter(shoe => shoe.id != id);
            setHats(result);
            console.log(response);
        }
    }


return (
    <table className="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Style Name</th>
                {/* <th>Color</th> */}
                {/* <th>Picture</th> */}
                <th>Location</th>
                {/* <th>Last Updated</th> */}
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
            {hats?.map(hat => {
                return (
                <tr key={hat.id}>
                    <td>{hat.id}</td>
                    <td>{hat.style_name}</td>
                    {/* <td>{hat.color}</td> */}
                    {/* <td>{hat.picture}</td> */}
                    <td>{hat.location.closet_name}</td>
                    {/* <td>{hat.updated}</td> */}
                    <td><button onClick={() => handleDelete(hat.id)} className="btn btn-primary">Delete</button></td>
                </tr>
                );
            })}
        </tbody>
    </table>
    )
}
export default HatsList;
