import React, { useEffect, useState } from 'react';

function HatForm(props) {
    const [formData, setFormData] = useState({
        fabric: '',
        style_name: '',
        color: '',
        picture_url: '',
        location: '',
    })
    const handleFormChange = async (event) => {
        const value = event.target.value;
        const name = event.target.name;
        setFormData( {...formData, [name]: value} );
    }
    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8090/api/location_value_objects/";
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setLocations(data.locations)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    const handleSubmit = async (event) => {
        event.preventDefault();

        console.log(formData);

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);
            setFormData({
                fabric: '',
                style_name: '',
                color: '',
                picture_url: '',
                location: '',
            })
        }
    }
return(

    <div className="row">
    <div className="offset-3 col-6">
    <div className="shadow p-4 mt-4">
        <h1>Create a new hat</h1>
        <form onSubmit={handleSubmit} id="create-hat-form">
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" value={formData.fabric} className="form-control"/>
            <label htmlFor="fabric">Fabric</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Style name" required type="text" name="style_name" id="pstyle_name" value={formData.style_name} className="form-control"/>
            <label htmlFor="Style name">Style Name</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Color" type="text" name="color" id="color" value={formData.color} className="form-control"/>
            <label htmlFor="color">Color</label>
        </div>
        <div className="mb-3">
            <input onChange={handleFormChange} placeholder="Picture URL" required type="url" name="picture_url" id="picture_url" value={formData.picture_url} className="form-control"/>
            <label htmlFor="picture_url">Picture URL</label>
        </div>
        <div className="mb-3">
            <select onChange={handleFormChange} required name="location" id="location" value={formData.location} className="form-select">
                <option value="">Choose a location</option>
                {locations.map((location) => {
                    return (
                        < option key={location.import_id} value={location.import_id}>
                            {location.closet_name}
                        </option>
                    )
                })}
            </select>
        </div>
        <button className="btn btn-primary">Create</button>
        </form>
    </div>
    </div>
</div>
)
}

export default HatForm;
