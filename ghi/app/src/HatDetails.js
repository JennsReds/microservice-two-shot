import React, { useEffect } from 'react';

function HatDetails(props) {

    let hat = {}
    const fetchData = async () => {
        const url = "http://localhost:8090/api/hats/:id";
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            hat = data;
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>
                    Attribute Name
                </th>
                <th>
                    Details
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th>ID</th>
                <td>{hat.id}</td>
            </tr>
            <tr>
                <th>Fabric</th>
                <td>{hat.fabric}</td>
            </tr>
            <tr>
                <th>Style Name</th>
                <td>{hat.style_name}</td>
            </tr>
            <tr>
                <th>Color</th>
                <td>{hat.color}</td>
            </tr>
            <tr>
                <th>Picture</th>
                <td>{hat.picture}</td>
            </tr>
            {/* <tr>
                <th>Location</th>
                <td>{hat.location.closet_name}</td>
            </tr> */}
            <tr>
                <th>Created</th>
                <td>{hat.created}</td>
            </tr>
            <tr>
                <th>Last Updated</th>
                <td>{hat.updated}</td>
            </tr>
            <tr>
                <th>Delete Hat</th>
                <td><button onClick={() => this.delete(hat.id)} className="btn btn-primary">Delete</button></td>
            </tr>
            </tbody>
        </table>
    )
}
export default HatDetails;



// const handleDelete = async (event) => {
//     event.preventDefault();

//     const id = hat.id;

//     const deleteHatUrl = 'http://localhost:8001/api/attendees/'+id;
//     const fetchConfig = {
//         method: "delete",
//         body: JSON.stringify(data),
//         headers: {
//         'Content-Type': 'application/json',
//         },
//     };

//     const response = await fetch(deleteHatUrl, fetchConfig);
//         if (response.ok) {
//             const deleteResponse = await response.json();
//             console.log(newAttendee);

//             return (
//                 "You have successfully deleted this hat"
//             )
//             }
//     }
