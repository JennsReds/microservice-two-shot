import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';
import HatsList from './HatsList';
import HatForm from './HatForm';
// import HatDetails from './HatDetails';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" index element={<MainPage />} />
          <Route path= "shoes">
            <Route path= "list" element= {<ShoesList shoes={props.shoes} />} />
            <Route path="new" element = {<ShoesForm />} />
          </Route>
          <Route path="hats" element={<HatsList />} />
          <Route path="hats">
              <Route path="new" element={<HatForm />} />
            </Route>
          {/* <Route path="hats">
            <Route path=":id" element={<HatDetails id={props.id}/>} />
          </Route> */}
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
