import React, {useEffect, useState,} from 'react';

function ShoesList() {
    const [shoes, setShoes] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/'
        const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                console.log(data)
                setShoes(data.shoes);
            } else {
                console.error(response)
            }
        }
    useEffect(() => {fetchData();}, []);

    const handleDelete = async (href) => {
        const response = await fetch(`http://localhost:8080${href}`,{ method: 'DELETE' });
        if (response.ok) {
            const result = shoes.filter(shoe => shoe.href != href);
            setShoes(result);
        };
    }


    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Picture</th>
                <th>Model Name</th>
                <th>Manufacturer</th>
                <th>Color</th>
                <th>ID</th>
                <th>Delete?</th>
            </tr>
            </thead>
            <tbody>
            {shoes.map(shoe => {
                return (
                <tr key={shoe.href}>
                    <td><img src={ shoe.picture_url } height="60px" width="60px" className="pic-thumb"/></td>
                    <td>{ shoe.model_name }</td>
                    <td>{ shoe.manufacturer }</td>
                    <td>{ shoe.color }</td>
                    <td>{shoe.id}</td>
                    <td>
                        <button onClick={() => handleDelete(shoe.href)}>Delete</button>
                    </td>

                </tr>
                );
            })}
            </tbody>
        </table>
    );
}

export default ShoesList;
