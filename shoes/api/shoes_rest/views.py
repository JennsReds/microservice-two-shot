from django.shortcuts import render
from .models import BinVO, Shoe
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "closet_name",]

class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "id"
        ]

class ShoesDetailEncoder(ModelEncoder):
    model= Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",

    ]
    encoders= {
        "bin": BinVODetailEncoder(),
    }

@require_http_methods(["GET","POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes=Shoe.objects.all()
        return JsonResponse(
            {"shoes":shoes},
            encoder=ShoesListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        print("content",content )

        try:
            bin_href= content["bin"]
            bin= BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin

        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin"},
                status=400,
                )
        shoe = Shoe.objects.create(**content),
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False,
        )
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            count,_ = Shoe.objects.filter(id=id).delete()
            return JsonResponse(
                {"deleted":count > 0}
                )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        content = json.loads(request.body)
        Shoe.objects.filter(id=id).update(**content)
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoesDetailEncoder,
            safe=False,
        )
